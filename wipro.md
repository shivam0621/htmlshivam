<!DOCTYPE html>
<html>
    <head>
        <style>
        td,th{
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        </style>
<script>
function validateStock() {
    var quantity = document.forms['stock']['qty'].value;
    quantity = parseInt(quantity);
    if(quantity < Math.min(10) || quantity > Math.max(100) || quantity % 10 !== 0)
        alert("INVALID QUANTITY");
}
</script>


    <body>
         <h2>STOCK TRADING</h2>
        <table>
            <tr>
            <th>TICKET</th>
            <th>PRICE</th>
            </tr>

            <tr>
                <td>WIPRO</td>
                <td>298.45</td>
            </tr>

            <tr>
                <td>INFY</td>
                <td>949.95</td>
            </tr>

            <tr>
                <td>TCS</td>
                <td>2713.70</td>
            </tr>
        </table>
    </body>

   
    <div>
            <form name="stock" onsubmit="validateStock()">
          
                <table style="border: 0;">
                    <tr>
                        <td style="border: 0;  text-transform: capitalize;">ticker:</td>
                        <td style="border: 0; text-transform: uppercase;"><input type="text" name="ticker"></input></td>
                    </tr>
                    <tr>
                        <td style="border: 0;  text-transform: capitalize;">quantity:</td>
                        <td style="border: 0;"><input type="text" name="qty"></input></td>
                    </tr>
                    <tr style="height: 12pt;"></tr>
                    <tr>
                        <td style="border: 0;"></td>
                        <td style="border: 0;"><input type="submit" name="submit" value="Submit"> </td>
                    </tr>
                </table>
            </form>
        </div>
        
        </body>
        </html>